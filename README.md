# isysftpserver
Ansible playbook to set up a sftp-server with hardening to upload backups (tested with ubuntu 18.04). 

## usage
Set up a clean lxc container with ubuntu 18.04. 

Attach the bind mount point with `pct set 100 -mp0 /data/backup,mp=/home` the associated container id. 
Allow root on proxmox:
`setfacl -Rm user:100000:rwx,default:user:100000:rwx`

To use the script install ansible and git:
`apt-get update`
`apt-get install ansible git`

Clone ansible-project with git (note: contains two subprojects): 
`git clone --recursive https://gitlab.com/vk-praxis/isysftpserver.git` 

Install required ansible galaxy roles: 
`ansible-galaxy install dev-sec.ssh-hardening` 

Run playbook: 
`ansible-playbook -i hosts playbook.yml` 

## useful commands 

See banned ips: 

`fail2ban-client status | sed -n 's/,//g;s/.*Jail list://p' | xargs -n1 fail2ban-client status` 

Unban an ip: 

`fail2ban-client set sshd unbanip ip_here`
